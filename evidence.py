import numpy as np
import pandas as pd

df= pd.read_csv("data.csv")  #LOAD FILE

total_count = df['SysCellTempHi'].count()   #count total data points

temp1 = df['SysCellTempHi']/10   #scaling of sensor

#counting the instance when sensor crossed threshold(30)
Count = 0 
for i in temp1 : 
    if i >30 : 
        Count = Count +1 
        
Count

#time spent 
time1 = (Count*100)/total_count
time = round(time1,3)

print('Instance when SysCellTempHi > 30 degree celcius =', Count ,)
print('Time spent =',time,'% of total time')

#if want to save FILE
df1 = df[(df['SysCellTempHi']>30)]

df1.to_csv('conditionNew.csv', index=False)