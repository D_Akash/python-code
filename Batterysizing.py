import numpy as np
import pandas as pd
dt= pd.read_csv("SOC.csv")


dt['SOC'] = dt.apply(lambda row: (row.ESVOLTARACKMODEL0104SOC)*100, axis = 1) 
total_SoC = dt['SOC'].count()
Min_SoC = 25
max_SoC = 75

#calculating time spent by Battery in Range
Count = 0
for i in dt['SOC'] : 
    if  Min_SoC < i < max_SoC : 
        Count = Count +1 
#print(Count) 
btw_range_per = ((Count* 100 )/total_SoC).round(3)
btw_range_per

#calculating time spent by Battery below Min-SOC
Count = 0
for i in dt['SOC'] : 
    if i < Min_SoC : 
        Count = Count +1 
#print(Count)        
Time_spent_low_soc = ((Count* 100 )/total_SoC).round(3)
Time_spent_low_soc

#calculating time spent by Battery over maximum SoC
Count = 0
for i in dt['SOC'] : 
    if i > max_SoC : 
        Count = Count +1 
#print(Count)        
Time_spent_high_soc = ((Count* 100 )/total_SoC).round(3)
Time_spent_high_soc

#calculating difference in soc below 25
new_results = {}
for index, row in dt.iterrows():  
   
        row['d'] = (Min_SoC - row['SOC']) if row['SOC'] < Min_SoC else 0
        new_results[index] = dict(row)    
        
dt = pd.DataFrame.from_dict(new_results, orient='index')

#calculating difference in SoC above 75
new_results = {}
for index, row in dt.iterrows():  
   
        row['e'] = (row['SOC']- max_SoC ) if row['SOC'] > max_SoC else 0 
        new_results[index] = dict(row)    
        
dt = pd.DataFrame.from_dict(new_results, orient='index')   

#calculating mean of difference 
mean_low_soc = round(dt['d'].mean(), 3)

mean_high_soc =  round(dt['e'].mean(), 3)

#setting limit for minimum  data
min_data= 500000

if Time_spent_low_soc < btw_range_per and Time_spent_high_soc < btw_range_per :
    print( '                   Warning : Insufficient Data may cause variation in result  ')
    print('Battery Capacity is in Good operating Region and spent',btw_range_per,'% time in Good Operating Region ')   
    
elif btw_range_per < mean_low_soc and   Time_spent_high_soc < Time_spent_low_soc :
    print( '                   Warning : Insufficient Data may cause variation in result  ')
    print('Battery is',mean_low_soc,'% Under-capacity and spent',Time_spent_low_soc,'% time of the Total time below ', Min_SoC, 'SoC' )
    
elif Time_spent_low_soc < Time_spent_high_soc  and     btw_range_per  < Time_spent_high_soc :
    print( '                   Warning : Insufficient Data may cause variation in result  ')
    print('Battery is',mean_high_soc,'% Over-capacity and spent',Time_spent_high_soc,'% time of the Total time above',max_SoC, 'SoC')