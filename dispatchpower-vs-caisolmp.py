import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

df1= pd.read_csv("data1.csv")      #caiso data

df1 = df1[((df1['LMP_TYPE']) == 'LMP')]    #filtering row - only LMP values
df1 = df1.loc[:, ['MW']]    #filtering- keeping only MW($/MW) column
df1 = df1.reset_index(drop=True)   #reset index

df2= pd.read_csv("data2.csv")      #esVolta Data
df2 = df2.loc[:, ['LastUpdateTime','OutputACEnergy']]
import datetime 
df2['Hours'] = df2['LastUpdateTime'].values.astype('<M8[h]')
df2['SoC'] = (df2['OutputACEnergy']/8000)*100

#get unique group of streak
h = df2['Hours'].ne(df2['Hours'].shift()).cumsum()
#find avg temp. of each unique streak
df2['avgSoC'] = df2.groupby(h)['SoC'].transform('mean')

df2 = df2.loc[:, ['Hours','avgSoC']]
df2 = df2.drop_duplicates(subset=['Hours',], keep='first')
df2 = df2.reset_index(drop=True)
df3 = df2.copy()

df3['MW'] = df1['MW']
df3 =  df3.rename(columns={"MW":"Caiso Bid Price($)","avgSoC":"esVolta avg SoC"})

df3.plot(x="Hours", y=["Caiso Bid Price($)", "esVolta SoC"])

x = df3['Hours']
y1 = df3['Caiso Bid Price($)']
y2 = df3['esVolta SoC']

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

ax1.plot(x, y1, 'g-')
ax2.plot(x, y2, 'b-')

ax1.set_xlabel('Hours')
ax1.set_ylabel('Caiso Bid Price($)', color='g')
ax2.set_ylabel('esVolta SoC', color='b')

plt.show()
