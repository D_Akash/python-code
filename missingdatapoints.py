import numpy as np
import pandas as pd
import datetime

#Frequency check
V_df= pd.read_csv("  .csv")      #verified dataframe
V_df['Time'] = pd.to_datetime(V_df['LastUpdateTime']).values.astype(np.int64) //10**6    #convert datetime to timestamp
V_Freq = round(V_df['ts'].diff().mean(),2)         # calculating frequency 

N_df= pd.read_csv("   .csv")       #New Data frame
N_df['Time'] = pd.to_datetime(N_df['LastUpdateTime']).values.astype(np.int64) //10**6    #convert datetime to timestamp
N_Freq = round(N_df['Time'].diff().mean(),2)          # calculating frequency 
if   V_Freq-(V_Freq*0.01) < N_Freq < V_Freq+ (V_Freq*0.01):
    print('No Action')
else :
    print('View/Jump Visualization page')
    

#Datapoints check
N_df['Missing Data points'] =N_df.isnull().sum(axis=1)                #count of NaN value