import numpy as np
import pandas as pd
import datetime

df= pd.read_csv("data.csv")      # dataframe

df['month'] = pd.to_datetime(df['date']).dt.to_period('M')

#keep the last value of each streak 
df = df.drop_duplicates(subset=['month',], keep='last')
df['date'] = pd.to_datetime(df['date'])   
df['Dates'] = pd.to_datetime(df['date'], format='%Y:%M:%D').dt.date   #separate date from datetime
df['start_date'] = '2017-11-1'       #start date of the data
df['start_date']= pd.to_datetime(df['start_date']).dt.date    #putting in dataframe
df['months'] = ((df.Dates - df.start_date)/np.timedelta64(1, 'M')).astype(int)   #subtracting two date and finding the difference

#filtering rows-keeping only 12,24,36
new_results = {}
for index, row in df.iterrows():  
   
        row['rem'] = 'True' if row['months']%12 == 0  else 0 
        new_results[index] = dict(row)    
        
df = pd.DataFrame.from_dict(new_results, orient='index')   

df = df.loc[:, ['ESVOLTARACKMODEL0104ExportedEnergy','months']]  #filtering column


#new dataframe- given by customer
#make dataframe if not available or load file if available
#if file available
df1 =  pd.read_csv(" .csv")
#merge df1 and df on months
dfinal = df.merge(df1, on="months", how = 'inner')

#if file not available
#make a dataframe manually
df3 = pd.DataFrame()
df3['month_comm'] = (12,24)
df3['exp_engy'] = (0,3075)
df3['Guaranteed dischargeable energy'] = (8000,8000)
df3['Minimal capacity for Dischargeable energy'] = (88.9,88.9)
df4 = df.merge(df3, how='inner', left_on='months', right_on='month_comm') #merge and put into new dataframe
df4['EE'] = df4['ESVOLTARACKMODEL0104ExportedEnergy']/1000  

#compare
new_results = {}
for index, row in df4.iterrows():  
   
        row['Energy Warranty'] = 'Warranty Not Violated' if row['EE'] <= row['exp_engy']  else 'Warranty Violated'
        row['SoH Warranty'] = 'Warranty Not Violated' if row['ESVOLTARACKMODEL0104SOH']*100 >= row['Minimal capacity for Dischargeable energy']  else 'Warranty Violated'
        row['Discharge Energy Guarantee'] = 'Warranty Not Violated' if row['esVolta Discharged energy'] >= row['Guaranteed dischargeable energy'] else 'Warranty Violated'
        new_results[index] = dict(row)    
        
df4 = pd.DataFrame.from_dict(new_results, orient='index')   

#filtering
dfinal = df4.loc[:, ['ESVOLTARACKMODEL0104ExportedEnergy','months','esVolta Discharged energy','exp_engy','Guaranteed dischargeable energy','Minimal capacity for Dischargeable energy','Energy Warranty','SoH Warranty','Discharge Energy Guarantee']]

#save output
dfinal.to_csv("warranty.csv", index = False)