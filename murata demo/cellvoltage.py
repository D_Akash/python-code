import numpy as np
import pandas as pd

df= pd.read_csv("MURATA_V10000CV.csv")    
df1 = df.copy()

df1 = df1[:12263]
df1 = df1.fillna(round(df1['cell_voltage__0'].mean(),1))

df2 = df1.copy()

median = df2['cell_voltage__0'].median()
std = df2['cell_voltage__0'].std()
outliers = (df2['cell_voltage__0'] - median).abs() > std
df2[outliers] = np.nan
df2['cell_voltage__0'].fillna(median, inplace=True)

df2 = df2.fillna(round(df1['cell_voltage__0'].mean(),1))

#1st 

df1a = df1.copy()

#rename
df1a = df1a.rename(columns={"cell_voltage__9": "cell_voltage__09"})
df1a = df1a.rename(columns={"cell_voltage__0": "cell_voltage__9"})
df1a = df1a.rename(columns={"cell_voltage__09": "cell_voltage__0"})

#3rd

df1c = df1.copy()

#rename
df1c = df1c.rename(columns={"cell_voltage__5": "cell_voltage__05"})
df1c = df1c.rename(columns={"cell_voltage__0": "cell_voltage__5"})
df1c = df1c.rename(columns={"cell_voltage__05": "cell_voltage__0"})

#merge
dt1 = pd.concat([df1,df2,df2,df2,df1c,df2,df2,df2,df1a,df2,df2], ignore_index=True)  #merge 
dt = dt1.copy()
dt = dt/1000

dt=dt.rename(columns={"cell_voltage__0":"ls1|0" ,"cell_voltage__1":"ls1|1", "cell_voltage__2":"ls1|2", "cell_voltage__3":"ls1|3",
                      "cell_voltage__4":"ls1|4", "cell_voltage__5":"ls1|5", "cell_voltage__6":"ls1|6", "cell_voltage__7":"ls1|7",
                     "cell_voltage__8":"ls1|8" ,"cell_voltage__9":"ls1|9" , "cell_voltage__10":"ls1|10", "cell_voltage__11":"ls1|11",
                     "cell_voltage__12":"ls1|12", "cell_voltage__13":"ls1|13"})

#put date
dt['ts'] =pd.date_range(start='10/1/2019',periods=len(dt) ,freq='122s') #format - mm/dd/yyyy
dt['ts']= pd.to_datetime(dt['ts']).values.astype(np.int64) //10**6 

ts = dt['ts']
dt.drop(labels=['ts'], axis=1,inplace = True)
dt.insert(0, 'ts', ts)
dt.to_csv('MuCVS0.csv',index=None )  #save file

## Battery Voltage

dt1['tqR|0'] = dt1.sum(axis=1)
dt1 = dt1.loc[:, ['tqR|0']]

dt1['ts'] =pd.date_range(start='10/1/2019',periods=len(dt1) ,freq='122s') #format - mm/dd/yyyy
dt1['ts']= pd.to_datetime(dt['ts']).values.astype(np.int64) //10**6 

ts = dt1['ts']
dt1.drop(labels=['ts'], axis=1,inplace = True)
dt1.insert(0, 'ts', ts)
dt1.to_csv('MuBVS0.csv',index=None )  #save file










