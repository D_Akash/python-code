import numpy as np
import pandas as pd

df= pd.read_csv("MURATA_V10000B.csv")     
dt = pd.concat([df,df,df,df,df,df,df], ignore_index=True)  #merge 

#slice
dt = dt[:194772]

#put date
dt['ts'] =pd.date_range(start='10/1/2019',periods=len(dt) ,freq='1.4Min') #format - mm/dd/yyyy
dt['ts']= pd.to_datetime(dt['ts']).values.astype(np.int64) //10**6 
ts = dt['ts']
dt.drop(labels=['ts'], axis=1,inplace = True)
dt.insert(0, 'ts', ts)
dt=dt.rename(columns={"balancing__0":"QXg|0" ,"balancing__1":"QXg|1", "balancing__2":"QXg|2", "balancing__3":"QXg|3",
                      "balancing__4":"QXg|4", "balancing__5":"QXg|5", "balancing__6":"QXg|6", "balancing__7":"QXg|7",
                     "balancing__8":"QXg|8" ,"balancing__9":"QXg|9" , "balancing__10":"QXg|10", "balancing__11":"QXg|11",
                     "balancing__12":"QXg|12", "balancing__13":"QXg|13"})


dt.to_csv('Mubalancing(same).csv',index=None)                    
