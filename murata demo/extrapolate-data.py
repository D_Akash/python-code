
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#individual sensor

df= pd.read_csv("m0000.csv")    #load data

df =df.drop(columns=['date'])
df = df.rename(columns={"MURATA_V10000Distance traveled": "Distance_traveled"})

##extrapolate data
df1 = df.copy()

last = df1['Distance_traveled'].iloc[-1]
first = 0 
diff = first- last
Distance_traveled = df1['Distance_traveled'] - diff
df1['Distance_traveled'] = Distance_traveled

df2 = df1.copy()
Distance_traveled1 = df2['Distance_traveled'] - diff
df2['Distance_traveled'] = Distance_traveled1

df3 = df2.copy()
Distance_traveled2 = df3['Distance_traveled'] - diff
df3['Distance_traveled'] = Distance_traveled2

## merging Dataframe df4 and df3
dt = pd.concat([df,df1,df2,df3], ignore_index=True)  #merge 

dt['MURATA_V10000Current'] = dt['MURATA_V10000Current']/10
dt['MURATA_V10000SOC'] = dt['MURATA_V10000SOC']/100
dt['Distance_traveled'] = dt['Distance_traveled']/1000

dt['ts'] =pd.date_range(start='10/1/2019',periods=len(dt) ,freq='1.4Min') #format - mm/dd/yyyy
dt['ts']= pd.to_datetime(dt['ts']).values.astype(np.int64) //10**6 
ts = dt['ts']
dt.drop(labels=['ts'], axis=1,inplace = True)
dt.insert(0, 'ts', ts)

dt=dt.rename(columns={"MURATA_V10000Current": "CiD|0","Distance_traveled": "s7E|0",
                         "MURATA_V10000Running State": "p9z|0","MURATA_V10000SOC": "yuq|0",
                         "MURATA_V10000Short Circuit Current": "xIV|0" })

## Range 160
dt.to_csv('Mu160.csv',index=None)

##Range 80
dfa = dt.copy()
dfa['s7E|0'] = dfa['s7E|0'] - dfa['s7E|0']* .50
dfa.to_csv('Mu80.csv',index=None)

## Range 40
dfb = dt.copy()
dfb['s7E|0'] = dfb['s7E|0'] - dfb['s7E|0']*.75
dfb.to_csv('Mu40.csv',index=None)

## Range 100
dfc = dt.copy()
dfc['s7E|0'] = dfc['s7E|0'] - dfc['s7E|0']*0.375
dfc.to_csv('Mu100.csv',index=None)

## Range 120
dfd = dt.copy()
dfd['s7E|0'] = dfd['s7E|0'] - dfd['s7E|0']* .25
dfd.to_csv('Mu120.csv',index=None)

## Range 60
dfe = dt.copy()
dfe['s7E|0'] = dfe['s7E|0'] - dfe['s7E|0']* .625
dfe.to_csv('Mu60.csv',index=None)
















