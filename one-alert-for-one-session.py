import numpy as np
import pandas as pd
df1= pd.read_csv("data.csv")      
df = df1.copy()
df = df.loc[:, ['LastUpdateTime','SysCellTempHi']]
df['SysCellTempHi'] = df['SysCellTempHi']/10
df['SysCellTempHi1'] = df['SysCellTempHi']

mask = df.SysCellTempHi1 > 40
column_name = 'SysCellTempHi1'
df.loc[mask, column_name] = "X"

#counting streak
df['strk'] =df.groupby( 'SysCellTempHi1' ).cumcount() ######

#get unique groups for consecutive values
g = df['SysCellTempHi1'].ne(df['SysCellTempHi1'].shift()).cumsum()

#filter only x
m = df['SysCellTempHi1'] == 'X'

#column of min value of each streak for "X"
df['min_stk'] = df[m].groupby(g)['strk'].transform('min')

#drop nan row from min-strk
df = df[df['min_stk'].notna()]

#get unique group of streak
h = df['min_stk'].ne(df['min_stk'].shift()).cumsum()

#find max temp. of each unique streak
df['Max_temp'] = df.groupby(h)['SysCellTempHi'].transform('max')

#find min temp. of each unique streak
df['Min_temp'] = df.groupby(h)['SysCellTempHi'].transform('min')


#if needed
#find median temp. of each unique streak
#df['Median_temp'] = df.groupby(h)['SysCellTempHi'].transform('median')

#find mean temp. of each unique streak
#df['Mean_temp'] = df.groupby(h)['SysCellTempHi'].transform('mean')

#find std temp. of each unique streak
#df['std_temp'] = df.groupby(h)['SysCellTempHi'].transform('std')

#last value of each streak  
df['Final Value'] = df.groupby(h)['SysCellTempHi'].transform('last')

#convert datetime to epoch
df['ts'] = pd.to_datetime(df['LastUpdateTime']).values.astype(np.int64) //10**6 

#first timestamp of each streak
df['timemax'] = df.groupby(h)['ts'].transform('max')

#last timestamp of each streak
df['timemin'] = df.groupby(h)['ts'].transform('min')

#get time difference or time spent by each streak
df['timediff'] = df['timemax'] - df['timemin']

#convert time spent into seconds or in time format
df['time_spent'] = pd.to_datetime(df['timediff'], unit='ms').dt.time 

#keep the first value of each streak and maximum of temp 
result_df = df.drop_duplicates(subset=['min_stk',], keep='first')

result_df['Condition of violation'] = 40

final_result = result_df.loc[:, ['LastUpdateTime','Condition of violation','SysCellTempHi','Max_temp','Min_temp','Median_temp','Mean_temp','std_temp','time_spent','Final Value']]

