#SHIFTING DATA
#import library
import numpy as np
import pandas as pd

#load file
df= pd.read_csv("data.csv")

#shift data as per requirement
df1= pd.DataFrame( columns=[])    #new dataframe
df1['DispatchPower'] = (df['DispatchPower']) + (df['DispatchPower']*2)  #save ouput in new dataframe
df1['PCSPower'] = (df['PCSPower']) + (df['PCSPower']*2)
df1['DispatchPower'] = (df['DispatchPower']) + (df['DispatchPower']*2)
df1['PCSID'] = (df['PCSID']) 

#save files
df1.to_csv('shfited-Data.csv', index=None)


#NOISE filling

#load data
df= pd.read_csv("shifted-data.csv")

#Random number- make a column of Random number
df['randNumCol'] = (np.random.uniform(.030,-.030, size=len(df))).round(3)

#multiply random number with sensors as per requirement
df['CmdPowerSetting'] = ((df['CmdPowerSetting']) + (df['CmdPowerSetting']*df['randNumCol'])).round(2)
df['PCSPower'] = ((df['PCSPower']) + (df['PCSPower']*df['randNumCol'])).round(2)
df['PCSID'] = ((df['PCSID']))
df['DispatchPower'] = ((df['DispatchPower']) + (df['DispatchPower']*df['randNumCol'])).round(2)

#save file
df.to_csv('Noise-Data.csv', index=None)


#Replicate data

#load file
df2= pd.read_csv("Noise-data.csv")

#reverse the data
reversed_df2 = df2.iloc[::-1]
reversed_df2.reset_index(drop=True, inplace=True)

#replicating and extending
dt = pd.concat([df2,reversed_df2,df2,reversed_df2,df2,reversed_df2,df2,reversed_df2,df2,reversed_df2,df2,reversed_df2],ignore_index=True)  

#fill datetime
dt['updatedtime'] =pd.date_range(start='1/1/2019',periods=len(dt) ,freq='6S') #end='1/1/2021'

#convert datetime to epoch
dt['ts'] = pd.to_datetime(dt['updatedtime']).values.astype(np.int64) //10**6 

#making timestamp column 1
ts = dt['ts']
dt.drop(labels=['ts'], axis=1,inplace = True)
dt.insert(0, 'ts', ts)

#drop extra column(here updatedtime)
dt =dt.drop(columns=['updatedtime'])

#save file
dt.to_csv('finalfile.csv', index=None)